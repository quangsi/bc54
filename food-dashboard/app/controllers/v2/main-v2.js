import { https } from "../../service/service.js";
import { layThongtinTuForm } from "../v1/controller-v1.js";
import { renderFoodList, showDataForm } from "./controller-v2.js";
let fetchFoodList = () => {
  https
    .get("/food")
    .then((res) => {
      renderFoodList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
// lần đầu load trang
fetchFoodList();
function deleteFood(id) {
  https
    .delete(`/food/${id}`)
    .then((res) => {
      fetchFoodList();
      // sau khi xoá thành công => gọi lại api lấy data mới nhất => update layout
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}
window.deleteFood = deleteFood;

window.addFood = () => {
  let food = layThongtinTuForm();
  https
    .post("/food", food)
    .then((res) => {
      $("#exampleModal").modal("hide");
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.editFood = (id) => {
  /**
   * 1. mở modal
   * 2. gọi api lấy chi tiết => https.get
   * 3. show response lên form
   *
   */
  // b1
  $("#exampleModal").modal("show");
  // b2
  https
    .get(`/food/${id}`)
    .then((res) => {
      console.log(res);
      showDataForm(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
